var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    uniqueValidator = require('mongoose-unique-validator');

var userSchema = Schema({
    username: {type: String, required: true, index: {unique: true, lowercase: true}},
    password: {type: String, required: true},
    auth_token: {type: String, required: true, min: 32, max: 32, index: {unique: true}},
    profile: Schema.Types.Mixed,
    createdAt: {
        type: Date,
        default: Date.now
    }
});

userSchema.plugin(uniqueValidator, {message: '{{PATH}} "{VALUE}" уже занят.'});

module.exports = mongoose.model('User', userSchema);