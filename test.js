var express = require('express'),
    app = express(),
    http = require('http'),
    server = http.createServer(app),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser')
    , passport = require('passport')
    , BearerStrategy = require('passport-http-bearer').Strategy
    , user = require('./index.js')
    , User = require('./models/user');

passport.use(new BearerStrategy(
    function(token, done) {
        User.findOne({ auth_token: token }, function (err, user) {
            if (err) { return done(err); }
            if (!user) { return done(null, false); }
            return done(null, user, { scope: 'all' });
        });
    }
));

mongoose.connect('mongodb://127.0.0.1:27017/nodejs_user', function (err) {
    if (err) throw err;

    app.use(passport.initialize());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use('/user', user.routes);
    app.use(function(err, req, res, next){
        console.log('ERRROR');
        res.json(err);
    });
    server.listen(8082);
    server.on('listening', function () {
        console.log('Express server started on port %s at %s', server.address().port, server.address());
    });
});