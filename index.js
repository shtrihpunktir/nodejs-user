var User = require('./models/user')
    , randToken = require('rand-token')
    , passwordHash = require('password-hash')
    , routes = require('./routes/index');

module.exports = {
    User: User,
    routes: routes,
    create: function(fields, notification){
        var m = new User(fields);
        m.password = passwordHash.generate(fields.password);
        m.auth_token = randToken.generate(32);
        return m.save().then(function(result){
            if (notification && notification.notify) {
                notification.notify('nodejs_user_created', {
                    message: 'User created',
                    user: result,
                    plainPassword: fields.password
                });
            }
            return result;
        });
    },    
    changePassword: function(userId, password, newPassword){
        return User.findById(userId).then(function(user){
            if (!user) {
                throw new Error('user_not_found');
            } else if (!passwordHash.verify(password, user.password)){
                throw new Error('password_mismatch');
            } else if (!newPassword) {
                throw new Error('password_empty');
            } else {                
                user.password = passwordHash.generate(newPassword);   
                return user.save();
            }
        })        
    },
    setPassword: function(userId, newPassword){
        return User.findById(userId).then(function(user){
            if (!user) {
                throw new Error('user_not_found');
            } else if (!newPassword) {
                throw new Error('password_empty');
            } else {                
                user.password = passwordHash.generate(newPassword);   
                return user.save();
            }
        })        
    },
};