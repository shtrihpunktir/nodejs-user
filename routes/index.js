var express = require('express')
    , router = express.Router()
    , randToken = require('rand-token')
    , passwordHash = require('password-hash'),
    passport = require('passport')
    , User = require('../models/user')
    , restExpress = require('rest-express');

// регистрация пользователя
router.post('/signup', function (req, res, next) {
    // TODO: captcha
    require('../index').create(req.body, req.app.get('notification'))
        .then(function (user) {
            res.statusCode = 201;
            return res.json(user);
        }, function (error) {
            var response = {status: "Error", message: error.message};
            if ("ValidationError" == error.name) {
                res.statusCode = 422;
                response["errors"] = error.errors;
            } else {
                res.statusCode = 400;
            }
            return res.json(response);
        });
});

// вход (логин) пользователя
router.post('/signin', function (req, res, next) {
    var username = req.body.username,
        password = req.body.password;
    // TODO: проверка captcha

    return User.findOne({username: {$regex: new RegExp('^'+username+'$'), $options: 'i'}}).then(function (user) {
        if (!user) {
            res.statusCode = 401;
            return res.json({status: "Error", message: "User not found"});
        }
        if (passwordHash.verify(password, user.password)) {
            return res.json(user.auth_token);
        }
        res.statusCode = 401;
        return res.json({status: "Error", message: "Invalid password"});
    }, function (error) {
        res.statusCode = 400;
        return res.json({status: "Error", message: error.message});
    });
});

// получить профиль пользователя
router.get('/profile', passport.authenticate('bearer', {session: false}), function (req, res, next) {
    var profile = req.user.profile || {};
    profile['id'] = req.user._id;
    return res.json(profile);
});

var controller = restExpress.controller(User);
controller.create = function (req, res, next) {
    require('../index').create(req.body, req.app.get('notification'))
        .then(function (result) {
            res.status(201);
            res.json(result);
            return result;
        }, next);
};

controller.update = function (req, res, next) {
    return User.findById(req.params.id || req.query.id).then(function(user){
        user.profile = req.body.profile;
        return user.save().then(function(savedUser){
            res.status(200);
            return res.json(savedUser);
        }, function (error) {
            var response = {status: "Error", message: error.message};
            if ("ValidationError" == error.name) {
                res.statusCode = 422;
                response["errors"] = error.errors;
            } else {
                res.statusCode = 400;
            }
            return res.json(response);
        });
    }, next);
};

restExpress.urlRule(router, '', controller);

module.exports = router;